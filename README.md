# Lab : Création et gestion d'un DaemonSet dans Kubernetes

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectifs 

1. Créer un fichier YAML de spécification DaemonSet

2. Créer le DaemonSet dans le cluster

# Contexte 

Vous travaillez pour BeeHive, une entreprise qui assure des expéditions régulières d'abeilles aux clients.

L'entreprise utilise un logiciel sur ses nœuds de travail K8 qui laisse périodiquement des données inutiles à un emplacement particulier sur le nœud de travail. Étant donné que ce logiciel peut s'exécuter sur n'importe quel nœud à tout moment, ces données indésirables apparaissent périodiquement sur différents nœuds de travail et doivent être nettoyées.

Configurez le cluster pour créer un pod sur chaque nœud de travail qui supprimait périodiquement le contenu du `/etc/beebox/tmp` nœud de travail. Assurez-vous qu'une copie de ce pod est automatiquement créée sur chaque nœud de travail, même lorsque de nouveaux nœuds sont ajoutés au cluster. Notez que vous ne devez pas exécuter une copie du pod sur le nœud du plan de contrôle.

Pour ce faire, vous pouvez utiliser la spécification de pod suivante comme modèle :

```yaml
spec:
  containers:
  - name: busybox
    image: busybox:1.27
    command: ['sh', '-c', 'while true; do rm -rf /beebox-temp/*; sleep 60; done']
    volumeMounts:
    - name: beebox-tmp
      mountPath: /beebox-temp
  volumes:
  - name: beebox-tmp
    hostPath:
      path: /etc/beebox/tmp
```

>![Alt text](img/image.png)

# Application 

Suivez les étapes ci-dessous pour créer et gérer un DaemonSet dans votre cluster Kubernetes.

### 1. Connexion au serveur de laboratoire

Connectez-vous au serveur de laboratoire à l'aide des informations d'identification fournies :

```sh
ssh -i id_rsa usuer@PUBLIC_IP_ADDRESS_CONTROL_NODE
```

### 2. Création d'un fichier YAML de spécification DaemonSet

Créez un descripteur DaemonSet en utilisant un éditeur de texte :

```sh
nano daemonset.yml
```

Ajoutez le contenu suivant au fichier :

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: beebox-cleanup
spec:
  selector:
    matchLabels:
      app: beebox-cleanup
  template:
    metadata:
      labels:
        app: beebox-cleanup
    spec:
      containers:
      - name: busybox
        image: busybox:1.27
        command: ['sh', '-c', 'while true; do rm -rf /beebox-temp/*; sleep 60; done']
        volumeMounts:
        - name: beebox-tmp
          mountPath: /beebox-temp
      volumes:
      - name: beebox-tmp
        hostPath:
          path: /etc/beebox/tmp
```

Enregistrez et quittez le fichier

### 3. Création du DaemonSet dans le cluster

Créez le DaemonSet dans le cluster avec la commande suivante :

```sh
kubectl apply -f daemonset.yml
```

>![Alt text](img/image-1.png)
*Daemonset crée*

### 4. Vérification des pods du DaemonSet

Obtenez une liste de pods et vérifiez qu'un pod DaemonSet est en cours d'exécution sur chaque nœud de travail :

```sh
kubectl get pods -o wide
```

>![Alt text](img/image-2.png)
*Pod actif*

L'on peut constater que le daemonSet deployé à bien crée des pods dans chaque node du cluster.

## Résumé

Dans ce lab, vous avez appris à :

1. Vous connecter à un serveur de laboratoire.
2. Créer un fichier YAML de spécification pour un DaemonSet.
3. Appliquer ce fichier YAML pour créer le DaemonSet dans le cluster.
4. Vérifier que les pods du DaemonSet sont bien déployés sur chaque nœud de travail.




